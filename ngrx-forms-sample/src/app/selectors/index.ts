import { State, ItemState } from '../interfaces';
import { createSelector } from '@ngrx/store';


export const selectItemState = (state: State) => state.itemState;

export const selectItemForm = createSelector(selectItemState, (state: ItemState) => state.form);

export const selectItems = createSelector(selectItemState, (state: ItemState) => state.data);

export const selectAppViewModel = createSelector(
    selectItems, selectItemForm,
    (items, form) => ({
        items,
        form
    }));
