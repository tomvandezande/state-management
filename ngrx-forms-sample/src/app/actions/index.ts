import { createAction } from '@ngrx/store';

export const addItem = createAction(
    '[Items] add new item'
);

export const clear = createAction(
    '[Items] clear items'
);
