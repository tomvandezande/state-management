import { Action, createReducer, on } from '@ngrx/store';

import { addItem, clear } from '../actions';
import { ItemState } from '../interfaces';
import { initialFormState, itemFormReducer } from './item-form.reducer';


const initialState = {
    data: [],
    form: initialFormState
};

const rawReducer = createReducer(
    initialState,
    on(addItem,
        (state: ItemState) => ({
            ...state,
            data: [...state.data, state.form.value.item],
        })
    ),
    on(clear,
        (state: ItemState) => ({
            ...state,
            data: [],
        })
    )
);

export const itemsReducer = (state: ItemState, action: Action) => {
    state = rawReducer(state, action);
    state = itemFormReducer(state, action);
    return state;
};
