import { Action, createReducer, on, createSelector } from '@ngrx/store';
import {
    createFormGroupState,
    disable,
    enable,
    formStateReducer,
    reset,
    setValue,
    updateGroup,
    validate,
    ValidationErrors,
} from 'ngrx-forms';
import { required } from 'ngrx-forms/validation';

import { addItem } from '../actions';
import { Item, ItemForm, ItemFormValues, ItemState } from '../interfaces';

const FORM_ID = 'New item form id';

const initialFormValues: ItemFormValues = {
    enforceUnique: false,
    item: ''
};

export const initialFormState = createFormGroupState<ItemFormValues>(FORM_ID, initialFormValues);

const unique = (items: Item[]) => (value: string): ValidationErrors =>
    items.includes(value) ? { unique: { actual: value } } : {};

const validateForm = (form: ItemForm, items: Item[]): ItemForm => {
    const checkUniqueIfApplicable = form.value.enforceUnique ? [unique(items)] : [];

    return updateGroup<ItemFormValues>(
        {
            item: validate([required, ...checkUniqueIfApplicable]),
        }
    )(form);
};

const updateForm = (form: ItemForm, itemsAreUnique: boolean): ItemForm => {
    return updateGroup<ItemFormValues>(
        {
            enforceUnique: itemsAreUnique ? enable : disable,
        }
    )(form);
};

const otherFormUpdates = createReducer(
    initialFormState,
    on(addItem,
        updateGroup<ItemFormValues>(
            { item: setValue('') },
            { item: reset }
        )
    )
);

const selectItemsFromItemState = (itemState: ItemState) => itemState.data;
const checkItemsUnique = (items: Item[]) => {
    console.log('calculating is unique');
    return (new Set(items)).size === items.length;
};
const selectItemsUnique = createSelector(selectItemsFromItemState, checkItemsUnique);

export const itemFormReducer = (state: ItemState, action: Action) => {
    // use the out of the box reducer to process form actions into the store
    let form = formStateReducer(state.form, action);

    // form validation using the new state
    form = validateForm(form, state.data);

    /**
     * In order to properly (en/dis)able the checkbox, we need to know if the list is unique.
     * This is a truth that lives in store, but outside of the form.
     * We define a function checkItemsUnique that returns wether the given list is unique:
     * ItemState -> boolean.
     * If we would directly use this function, we would be doing calculation on every form action.
     * Instead we wrap it in a selector, making it memoized.
     * I've put a console log in the checkItemsUnique. See what happens when you use this function
     * directly by switching out the lines below.
     */
    const isUnique = selectItemsUnique(state);
    // const isUnique = checkItemsUnique(state.data);
    form = updateForm(form, isUnique);

    // Process non-form actions. Just a personal convention to do this in a seperate reducer.
    form = otherFormUpdates(form, action);

    /**
     * All update functions (like enable, setValue etc) make sure that no object reference changes
     * if nothing in the object changes. We will do the same in the line below.
     * This enables us to use a very declarive style in our reducers.
     * For a more elaborate explanation read the documentation at
     * https://ngrx-forms.readthedocs.io/en/master/user-guide/updating-the-state/#declarative-vs-imperative-updates
     */
    state = form === state.form ? state : { ...state, form };
    return state;
};
