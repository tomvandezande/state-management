import { ActionReducerMap } from '@ngrx/store';

import { State } from '../interfaces';
import { itemsReducer } from './items.reducer';


export const reducers: ActionReducerMap<State> = {
  itemState: itemsReducer,
};
