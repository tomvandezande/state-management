import { FormGroupState } from 'ngrx-forms';

export interface ItemFormValues {
    enforceUnique: boolean;
    item: string;
}

export type ItemForm = FormGroupState<ItemFormValues>;
export type Item = string;

export interface ItemState {
    data: Item[];
    form: ItemForm;
}

export interface State {
    itemState: ItemState;
}
