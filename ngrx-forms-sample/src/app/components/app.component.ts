import { Component } from '@angular/core';
import { selectAppViewModel } from '../selectors';
import { Store, select } from '@ngrx/store';
import { State } from '../interfaces';
import { addItem, clear } from '../actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public vm$ = this.store.pipe(select(selectAppViewModel));

  public addItem() {
    this.store.dispatch(addItem());
  }

  public clear() {
    this.store.dispatch(clear());
  }

  public constructor(private readonly store: Store<State>) {
  }
}
