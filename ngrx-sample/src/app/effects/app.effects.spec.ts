import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable, of } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';

import { setFilter } from '../actions/filter.actions';
import { clear, loadItems, loadItemsSuccess } from '../actions/item.actions';
import { State } from '../reducers';
import { selectActiveFilter } from '../selectors';
import { SampleItemsService } from '../services/sample-items.service';
import { AppEffects } from './app.effects';


describe('[AppEffects]', () => {
  let store: MockStore<State>;
  let actions$: Observable<any>;
  let effects: AppEffects;
  let testScheduler: TestScheduler;
  let serviceMock: jasmine.SpyObj<SampleItemsService>;


  beforeEach(() => {
    testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });

    const spy = jasmine.createSpyObj('SampleItemService', ['call$']);

    TestBed.configureTestingModule({
      providers: [
        { provide: SampleItemsService, useValue: spy },
        AppEffects,
        provideMockActions(() => actions$),
        provideMockStore(),
      ]
    });

    store = TestBed.get(Store);
    serviceMock = TestBed.get(SampleItemsService);
    effects = TestBed.get<AppEffects>(AppEffects);

    serviceMock.call$.and.callFake(
      (filterId: string | undefined) => {
        const str = filterId || 'all';
        return of([{ id: str, value: str }]);
      });
  });

  const actionMarbles = {
    a: setFilter({ filterId: 'a' }),
    b: setFilter({ filterId: 'b' }),
    c: clear(),
    l: loadItems(),
    A: loadItemsSuccess({ data: [{ id: 'a', value: 'a' }] }),
  };

  const serviceMarbles = {
    a: [{ id: 'a', value: 'a' }]
  };

  describe('[loadItems$]', () => {
    describe('when active filter is "a"', () => {
      beforeEach(() => {
        store.overrideSelector(selectActiveFilter, 'a');
      });

      it('should return items for "a" on loadItems', () => {
        const source = '  -l';
        const expected = '-A';
        testScheduler.run(({ hot, expectObservable }) => {
          actions$ = hot(source, actionMarbles);
          expectObservable(effects.loadItems$).toBe(expected, actionMarbles);
        });
      });
      it('should ignore loadItems when a call is already running', () => {
        const response = '-a|';
        const source = '  -ll--';
        const expected = '--A--';

        testScheduler.run(({ cold, hot, expectObservable }) => {
          serviceMock.call$.and.returnValue(cold(response, serviceMarbles));
          actions$ = hot(source, actionMarbles);
          expectObservable(effects.loadItems$).toBe(expected, actionMarbles);
        });
      });
      it('should cancel running calls when cancel action is dispatched', () => {
        const response = '-a|';
        const source = '  -lc-l';
        const expected = '-----A';

        testScheduler.run(({ cold, hot, expectObservable }) => {
          serviceMock.call$.and.returnValue(cold(response, serviceMarbles));
          actions$ = hot(source, actionMarbles);
          expectObservable(effects.loadItems$).toBe(expected, actionMarbles);
        });
      });
    });
  });
  describe('[clearAndLoad$]', () => {
    describe('when active filter is "a"', () => {
      beforeEach(() => {
        store.overrideSelector(selectActiveFilter, 'a');
      });
      it('should load "b" only once if setFilter "b" is dispatched twice in a row', () => {
        const source = '  -b   b-';
        const expected = '-(cl)--';

        testScheduler.run(({ hot, expectObservable }) => {
          actions$ = hot(source, actionMarbles);
          expectObservable(effects.clearAndLoadItems$).toBe(expected, actionMarbles);
        });
      });
    });
  });


});
