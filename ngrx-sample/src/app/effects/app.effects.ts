import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, distinctUntilKeyChanged, exhaustMap, map, switchMap, takeUntil, withLatestFrom, tap } from 'rxjs/operators';

import { setFilter } from '../actions/filter.actions';
import { clear, loadItems, loadItemsFailure, loadItemsSuccess } from '../actions/item.actions';
import { SampleItemsService } from '../services/sample-items.service';
import { select, Store } from '@ngrx/store';
import { State } from '../reducers';
import { selectActiveFilter } from '../selectors';



@Injectable()
export class AppEffects {
  loadItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadItems),
      withLatestFrom(this.store.pipe(select(selectActiveFilter))),
      exhaustMap(([_, filterId]) => this.fetchFromService(filterId))
    )
  );

  clearAndLoadItems$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setFilter),
      distinctUntilKeyChanged('filterId'),
      switchMap(_ => [clear(), loadItems()])
    ));

  private fetchFromService(filterId?: string) {
    return this.sampleItemsService.call$(filterId).pipe(
      takeUntil(this.actions$.pipe(ofType(clear))),
      map(data => loadItemsSuccess({ data })),
      catchError(error => of(loadItemsFailure({ error })))
    );
  }

  constructor(private actions$: Actions, private store: Store<State>, private sampleItemsService: SampleItemsService) { }

}
