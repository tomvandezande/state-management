import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';

import { AppComponent } from './components/app/app.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './effects/app.effects';
import { ChipFilterComponent } from './components/chip-filter/chip-filter.component';
import { SampleItemsService } from './services/sample-items.service';

export const ROOT_REDUCER = new InjectionToken<any>('Root Reducer');

@NgModule({
  declarations: [
    AppComponent,
    ChipFilterComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(ROOT_REDUCER, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
      }
    }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects])
  ],
  providers: [
    {provide: ROOT_REDUCER, useValue: reducers},
    SampleItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
