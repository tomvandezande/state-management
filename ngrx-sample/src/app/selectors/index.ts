import { createSelector, MemoizedSelector } from '@ngrx/store';

import { State } from '../reducers';
import { ViewModel } from '../models/view-model.interface';


const selectDataModelState = (state: State) => state.dataModel;
export const selectItems = createSelector(selectDataModelState, (dataModel)  => dataModel.items);

const selectViewModelState = (state: State) => state.viewModel;
export const selectIsLoading = createSelector(selectViewModelState, (viewModel) => viewModel.isLoading);
export const selectActiveFilter = createSelector(selectViewModelState, (viewModel) => viewModel.activeFilter);

export const selectItemCount = createSelector(selectItems, (items) => items.length);

export const selectViewModel: MemoizedSelector<State, ViewModel> = createSelector(
    [selectItems, selectIsLoading, selectActiveFilter, selectItemCount],
    (items, isLoading, activeFilter, itemCount) => ({
        items,
        isLoading,
        activeFilter,
        itemCount
    })
);
