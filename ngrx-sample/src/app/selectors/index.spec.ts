import { selectViewModel } from '.';
import { SampleItem } from '../models/sample-item.interface';
import { ViewModel } from '../models/view-model.interface';
import { State } from '../reducers';

describe('selectViewModel', () => {
    const state: State = {
        viewModel: {
            isLoading: true,
            activeFilter: 'a'
        },
        dataModel: {
            items: []
        }
    };
    it('should return a viewmodel', () => {
        const loadingWithoutItems: State = { ...state };
        const expected: ViewModel = {
            items: [],
            isLoading: true,
            activeFilter: 'a',
            itemCount: 0,
        };
        expect(selectViewModel(loadingWithoutItems)).toEqual(expected);
    });
    it('should return viewmodel with correct itemCount', () => {
        const fakeItem = {} as SampleItem;
        const loadingWithoutItems: State = {
            ...state,
            dataModel: { items: [fakeItem, fakeItem] }
        };
        const expected = { itemCount: 2 };
        expect(selectViewModel(loadingWithoutItems)).toEqual(jasmine.objectContaining(expected));
    });
});

