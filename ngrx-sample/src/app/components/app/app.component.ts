import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { setFilter } from 'src/app/actions/filter.actions';
import { selectViewModel } from 'src/app/selectors';
import { State } from 'src/app/reducers';
import { loadItems } from 'src/app/actions/item.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  public filters = { 'Only A': 'a', 'Only B': 'b', 'All results': undefined };

  public viewModel$ = this.store.pipe(select(selectViewModel));

  constructor(private readonly store: Store<State>) {}

  filterChanged(value: any) {
    this.store.dispatch(setFilter({ filterId: value }));
  }

  loadMoreItems() {
    this.store.dispatch(loadItems());
  }

  ngOnInit(): void {
    this.store.dispatch(setFilter({ filterId: undefined }));
  }
}
