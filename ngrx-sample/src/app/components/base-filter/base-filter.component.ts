import { EventEmitter, Input, Output } from '@angular/core';

export class BaseFilterComponent<T>  {
  @Input() label = '';
  @Input() value!: T;
  @Output() update = new EventEmitter<T>();

  public setValue(value: T) {
    this.update.emit(value);
  }

  constructor() { }
}
