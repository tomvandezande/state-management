import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { BaseFilterComponent } from '../base-filter/base-filter.component';

@Component({
  selector: 'chip-filter',
  templateUrl: './chip-filter.component.html',
  styleUrls: ['./chip-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChipFilterComponent extends BaseFilterComponent<string | undefined> {
  @Input() chips: { [key: string]: string | undefined } = {};
}
