import { SampleItem } from './sample-item.interface';

export interface ViewModel {
    items: SampleItem[];
    isLoading: boolean;
    activeFilter: string | undefined;
    itemCount: number;
}
