import { dataModelReducer, DataModelState } from './data-model.reducer';
import { clear, loadItemsSuccess } from '../actions/item.actions';
import { SampleItem } from '../models/sample-item.interface';

describe('dataModelReducer', () => {
    describe('clear action', () => {
        it('should clear the items', () => {
            const state: DataModelState = {
                items: [{} as SampleItem]
            };
            const action =  clear();
            const expected: DataModelState = {
                items: []
            };
            expect(dataModelReducer(state, action)).toEqual(expected);
        });
    });
    describe('loadSuccess action', () => {
        it('should append items to state on loadSuccess action', () => {
            const fakeItem = {} as SampleItem;
            const action  = loadItemsSuccess({ data: [fakeItem] });
            const state: DataModelState = {
                items: [fakeItem]
            };
            const expected: DataModelState = {
                items: [fakeItem, fakeItem]
            };
            expect(dataModelReducer(state, action)).toEqual(expected);
        });
    });
});
