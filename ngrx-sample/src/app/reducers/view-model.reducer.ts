import { createReducer, on } from '@ngrx/store';
import { loadItemsFailure, loadItemsSuccess, loadItems, clear } from '../actions/item.actions';
import { setFilter } from '../actions/filter.actions';

export interface ViewModelState {
    isLoading: boolean;
    activeFilter?: string;
}

export const initialViewModel: ViewModelState = {
    isLoading: false,
    activeFilter: undefined
};

export const viewModelReducer = createReducer<ViewModelState>(
    initialViewModel,
    on(loadItemsFailure, loadItemsSuccess, clear,
        (vm) => ({
            ...vm,
            isLoading: false
        })
    ),
    on(loadItems,
        (vm) => ({
            ...vm,
            isLoading: true
        })
    ),
    on(setFilter,
        (vm, action) => ({
            ...vm,
            activeFilter: action.filterId
        })
    )
);
