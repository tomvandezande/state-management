import { clear, loadItemsSuccess, loadItemsFailure } from '../actions/item.actions';
import { SampleItem } from '../models/sample-item.interface';
import { dataModelReducer, DataModelState } from './data-model.reducer';
import { viewModelReducer, ViewModelState } from './view-model.reducer';
import { setFilter } from '../actions/filter.actions';

describe('viewModelReducer', () => {
    describe('clear action', () => {
        it('should set loading flag to false', () => {
            const state: ViewModelState = {
                isLoading: true
            };
            const action =  clear();
            const expected: ViewModelState = {
                isLoading: false
            };
            expect(viewModelReducer(state, action)).toEqual(expected);
        });
    });
    describe('loadSuccess action', () => {
        it('should set loading flag to false', () => {
            const state: ViewModelState = {
                isLoading: true
            };
            const action  = loadItemsSuccess({ data: [] });
            const expected: ViewModelState = {
                isLoading: false
            };
            expect(viewModelReducer(state, action)).toEqual(expected);
        });
    });
    describe('loadItemFailure action', () => {
        it('should set loading flag to false', () => {
            const state: ViewModelState = {
                isLoading: true
            };
            const action  = loadItemsFailure({ error: ''});
            const expected: ViewModelState = {
                isLoading: false
            };
            expect(viewModelReducer(state, action)).toEqual(expected);
        });
    });
    describe('setFilter action', () => {
        it('should set active filter', () => {
            const state: ViewModelState = {
                isLoading: false,
            };
            const action  = setFilter({ filterId: 'a' });
            const expected: ViewModelState = {
                ...state,
                activeFilter: 'a'
            };
            expect(viewModelReducer(state, action)).toEqual(expected);
        });
    });
});
