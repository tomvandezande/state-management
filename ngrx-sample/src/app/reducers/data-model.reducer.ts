import { createReducer, on } from '@ngrx/store';
import { SampleItem } from '../models/sample-item.interface';
import { loadItemsSuccess, clear } from '../actions/item.actions';

export interface DataModelState {
    items: SampleItem[];
}

export const initialDataModel = { items: [] };

export const dataModelReducer = createReducer<DataModelState>(
    initialDataModel,
    on(loadItemsSuccess,
        (state, action) => ({
            ...state,
            items: [...state.items, ...action.data]
        })
    ),
    on(clear,
        (state) => ({
            ...state,
            items: []
        })
    )
);
