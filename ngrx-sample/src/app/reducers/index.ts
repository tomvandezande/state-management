import { ActionReducerMap } from '@ngrx/store';
import { DataModelState, dataModelReducer } from './data-model.reducer';
import { ViewModelState, viewModelReducer } from './view-model.reducer';

export interface State {
  dataModel: DataModelState;
  viewModel: ViewModelState;
}

export const reducers: ActionReducerMap<State> = {
  dataModel: dataModelReducer,
  viewModel: viewModelReducer
};
