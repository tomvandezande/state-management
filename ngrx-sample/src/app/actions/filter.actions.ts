import { createAction, props } from '@ngrx/store';

export const setFilter = createAction(
  '[Filters] select filter',
  props<{ filterId: string | undefined }>()
);
