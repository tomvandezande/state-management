import { createAction, props } from '@ngrx/store';
import { SampleItem } from '../models/sample-item.interface';

export const loadItems = createAction(
  '[Items] Load Items',
);

export const loadItemsSuccess = createAction(
  '[Items] Load Items Success',
  props<{ data: SampleItem[] }>()
);

export const loadItemsFailure = createAction(
  '[Items] Load Items Failure',
  props<{ error: any }>()
);

export const clear = createAction(
  '[Items] clear items'
);

