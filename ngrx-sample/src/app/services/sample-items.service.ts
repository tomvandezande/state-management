import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { SampleItem } from '../models/sample-item.interface';


@Injectable()
export class SampleItemsService {
    private id = 0;

    public call$(filterId: string | undefined): Observable<SampleItem[]> {
        return of([1, 2, 3, 4, 5, 6, 7, 8, 9].map(_ => this.responseFactory(filterId || 'all')))
            .pipe(
                delay(500 + Math.random() * 1500),
            );
    }

    private responseFactory(filter: string): SampleItem {
        this.id = this.id + 1;
        return {
            id: `${this.id}`,
            value: filter + ' ' + this.randomString()
        };
    }

    private randomString() {
        return (Math.random() * 10).toString(36).replace(/[^a-z]*/, '');
    }

}
